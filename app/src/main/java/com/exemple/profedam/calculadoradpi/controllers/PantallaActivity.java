package com.exemple.profedam.calculadoradpi.controllers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.exemple.profedam.calculadoradpi.R;
import com.exemple.profedam.calculadoradpi.model.Pantalla;


public class PantallaActivity extends AppCompatActivity {
private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla);
        textView = findViewById(R.id.textView);
        Pantalla pantalla = (Pantalla) getIntent().getSerializableExtra("object");
        textView.setText ("La resolución es: " + pantalla.getDpi());
    }
}