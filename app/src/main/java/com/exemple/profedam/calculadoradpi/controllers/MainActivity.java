package com.exemple.profedam.calculadoradpi.controllers;


import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Surface;
import android.view.View;

import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.exemple.profedam.calculadoradpi.R;

import com.exemple.profedam.calculadoradpi.model.Pantalla;
import com.exemple.profedam.calculadoradpi.utils.MyUtils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Pantalla pantalla;
    private EditText etResHoritzontal, etResVertical, etDiagonal;
    private TextView tvDpis, tvResHoritzontal, tvResVertical, tvDiagonal, tvDpiH;
    private Button btnCalcular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
    /*    BatteryManager batteryManager = (BatteryManager) getSystemService(Context.BATTERY_SERVICE);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        */
         Display display = windowManager.getDefaultDisplay();

          if (display.getRotation()== Surface.ROTATION_0 || display.getRotation()==Surface.ROTATION_180)
          {
              cargarVertical();
          }

          else {

              cargarHorizontal(display);


          }






    }

    public void cargarHorizontal(Display display) {
        Toast.makeText (this, "Esto está por hacer", Toast.LENGTH_SHORT).show();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);


        int resH = displayMetrics.widthPixels;
        int resV = displayMetrics.heightPixels;
        int dpis = displayMetrics.densityDpi;

        pantalla = new Pantalla(resH, resV, dpis);
        double diagonal = pantalla.getDiagonal();

        tvDiagonal = findViewById(R.id.tvDiagonalED);
        tvResHoritzontal = findViewById(R.id.tvResolucionHorizontalED);
        tvResVertical = findViewById(R.id.tvResolucionVerticalED);
        tvDpiH = findViewById (R.id.tvPPIED);

        tvDiagonal.setText(String.valueOf(diagonal) + " pulgadas");
        tvResHoritzontal.setText(resH + " ");
        tvResVertical.setText(resV + " ");
        tvDpiH.setText(dpis + " ");
    }

    public void cargarVertical() {
        etResHoritzontal = findViewById(R.id.etResolucioHoritzontal);
        etResVertical = findViewById(R.id.etResolucioVertical);
        etDiagonal = findViewById(R.id.etDiagonal);
        tvDpis = findViewById(R.id.tvDpi);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnCalcular.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        if (!MyUtils.editTextIsNullOrEmpty(etResHoritzontal)
                && !MyUtils.editTextIsNullOrEmpty(etResVertical) &&
                !MyUtils.editTextIsNullOrEmpty(etDiagonal))

        {
            int resH = Integer.parseInt(etResHoritzontal.getText().toString());
            int resV = Integer.parseInt(etResVertical.getText().toString());
            double longitud = Double.parseDouble(etDiagonal.getText().toString());

            pantalla = new Pantalla(resH, resV, longitud);
            Intent i = new Intent (this, PantallaActivity.class);
            i.putExtra("object", pantalla);
            startActivity(i);


           //  tvDpis.setText(String.valueOf(pantalla.getDpi()) + " dpi");
        } else {
            if (MyUtils.editTextIsNullOrEmpty(etResHoritzontal)) {
                Toast.makeText(this, "Falta resolución horizontal", Toast.LENGTH_SHORT).show();

                //  etResHoritzontal.setFocusable(true);
                etResHoritzontal.requestFocus();

            }

            if (MyUtils.editTextIsNullOrEmpty(etResVertical)) {
                Toast.makeText(this, "Falta resolución vertical", Toast.LENGTH_SHORT).show();
                etResVertical.requestFocus();

            }

            if (MyUtils.editTextIsNullOrEmpty(etDiagonal)) {
                Toast.makeText(this, "Falta Diagonal", Toast.LENGTH_SHORT).show();
                etDiagonal.requestFocus();

            }
        }
    }












}


